# fab-inventory

A small application in python using [bokeh](https://bokeh.org/) to manage **small** inventories.

Two options for backup are available:
- Local (csv), with optional git repository backup
- Remote Google Spreadsheet (not the best, but bleh). You need to get API credentials if you are using this option. Follow instructions here: https://gspread.readthedocs.io/en/latest/oauth2.html

## Usage

**Fork** this repository and then **clone it**.

```
# Fork first!
git clone git@gitlab.fabcloud.org:yournewlyforkedinventory/fab-inventory.git
cd fab-inventory
```

If you are running this in a Raspberyy pi (tested on Rpi3b+):

``` 
# Make SSH keys for Git
cd ~/.ssh
chmod 700 ~/.ssh
ssh-keygen
cat id_rsa.pub

# Install git and clone repo
sudo apt-get update
sudo apt-get install git

# Install python 3.7
sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev
59  wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
60  sudo tar zxf Python-3.7.0.tgz
61  sudo ./configure

# Check we are running python 3.7
python3.7 -V

# Alias python3.7 to python
which python3.7
nano ~/.bashrc
# Relaunch bashrc
source ~/.bashrc
# And check everything works fine
python
# Install pip
sudo apt install python3-pip, virtualenv

# Clone Repo
cd Documents/
git clone git@gitlab.fabcloud.org:barcelonaworkshops/fab-inventory.git
cd fab-inventory/
# Create virtual environment
python -m venv environment
# Activate it
source environment/bin/activate
pip install -r requirements.txt

# Try to run 
cd ..
bokeh serve fab-inventory/
# If you get errors for numpy, try the following
sudo python3.7 -m pip install 'numpy>1.0, <1.15' --force-reinstall
# If you have library problems try
sudo apt-get install g++-4.7, libc6, libgfortran5, libopenjp2-7, libtiff5

# Install chromium
sudo apt-get install -y chromium-browser ttf-mscorefonts-installer unclutter x11-xserver-utils --allow-unauthenticated
sudo apt-get install lightdm

# If you are using ssh, run
export DISPLAY=:0.0
# Now this should work
chromium-browser --kiosk http://duckduckgo.com
```

![](assets/screen_capture.png)

## Raspberry pi extras

###  Screen

If you are using a pi with a touch screen, follow [instructions here](http://www.lcdwiki.com/7inch_HDMI_Display-C)

![](http://www.lcdwiki.com/images/7/76/MPI7002-01.png)

###  Start on boot

Create a service:
```
sudo nano /etc/systemd/system/dashboard.service
```

And put:

```
[Unit]
Description=Chromium Dashboard
Requires=graphical.target
After=graphical.target

[Service]
Environment=DISPLAY=:0.0
Environment=XAUTHORITY=/home/pi/.Xauthority
Type=simple
ExecStart=/home/pi/dashboard.sh
Restart=on-abort
User=pi
Group=pi

[Install]
WantedBy=graphical.target
```

Where dashboard.sh:

```
#!/bin/bash
xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/pi/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/pi/.config/chromium/Default/Preferences

/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk http://localhost:5006/fab-inventory &
/home/pi/Documents/fab-inventory/environment/bin/bokeh serve /home/pi/Documents/fab-inventory
```

And enable it:

```
chmod +x /home/pi/dashboard.sh
systemctl enable dashboard.service
```

## Configuration file:

There is a `config.yml` in the project root folder to allow for different configurations:

- origin: where the source of truth for this application is
    + type: `gspreadsheet` or `csv`. in case of `csv`, it should be in `data/inventory.csv`
    + url: in case of `gspreadsheet`, the url of your inventory
    + number_fields: how many fields (columns) your inventory has. Some people are messier than other
- backup:
    + incremental_save: whether or not to keep a backup copy with the timestamp saved
    + save_locally: in case of `gspreadsheet`, whether or not to save a copy locally under `data/inventory.csv`
    + repo: in case of csv, whether or not you want to commit this inventory and push it to have a failsafe backup online
- logging:
    + save_locally: whether or not to keep a log on `logfile.log`

Example:

```
origin: 
  type: 'gspreadsheet'
  url: 'https://docs.google.com/spreadsheets/d/fakeurl'
  number_fields: 7
backup:
  incremental_save: true
  save_locally: true
  repo: 'git.fake.repo'
logging: 
  save_locally: true
```

## Customize it

In the templates folder you can find styles.css and index.html if you want to customise it.

