from bokeh.models import Panel, Button, Div
from bokeh.layouts import column, row
from bokeh.events import ButtonClick
import multiprocessing

def submit_tab (database):

	def callback_submit(event):
		p = multiprocessing.Process(target=database.request('submit'))
		# database.request('submit')
		p.start()
		p.join(10)
		if p.is_alive():
			print ("running... let's kill it...")

			# Terminate
			p.terminate()
			p.join()

	def callback_clear(event):
		database.request('clear')

	button_submit = Button(label="Submit", button_type="success", width=470)
	button_submit.on_event(ButtonClick, callback_submit)
	# Button click
	button_clear = Button(label="Clear", button_type="danger", width=470)
	button_clear.on_event(ButtonClick, callback_clear)
	data_table = database.datatable
	layout = column(data_table, row(button_submit, button_clear))
	tab = Panel(child = layout, title = 'Submit')

	return tab