from bokeh.models import Panel, Button
from bokeh.models.widgets import Div, Paragraph
from bokeh.layouts import column, row

def admin_tab (database):

	def callback_reload():
		database.request('reload')

	div = Div(text="""<p id="description">
	  	Click below to reload the database from source
	  </p>""", width=900, height=50)
	button_reload = Button(label="Reload database", button_type="danger", width = 900)
	button_reload.on_click(callback_reload)
	
	layout = column(div, button_reload)
	tab = Panel(child = layout, title = 'Admin')

	return tab