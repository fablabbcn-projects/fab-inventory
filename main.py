from tabs.submit import submit_tab
from tabs.admin import admin_tab

## TODO
'''
[X] Put tabs back into separate files
[ ] Add way to keep a backlog in case submit times out and doesn't actually push
'''

from time import gmtime, strftime, localtime

import subprocess
import multiprocessing
from multiprocessing import Queue
from os.path import dirname, join, exists
import os
from time import strftime, localtime
import yaml
import pandas as pd
import numpy as np
import traceback

# BOKEH STUFF
from bokeh.io import curdoc
from bokeh.layouts import column, row, layout
from bokeh.server.server import Server
from bokeh.embed import server_document
from bokeh.models import (Button, ColumnDataSource, DataTable, Panel, TableColumn, Dropdown, MultiSelect)
from bokeh.events import ButtonClick
from bokeh.models.widgets import Select, Div, Tabs
from tornado import gen
from tornado.ioloop import IOLoop
from flask import Flask, render_template

app = Flask(__name__)

class DataBaseHandler():

	def __init__(self, verbose = True):
		self.verbose = verbose

		try:
			config_path = join(dirname(__file__), 'config.yml')
			with open(config_path, 'r') as config_yaml:
				self.config = yaml.load(config_yaml, Loader=yaml.SafeLoader)
		except:
			traceback.print_exc()
			raise SystemError('Problem loading configuration file')
		
		self.logging = self.config['logging']['save_locally']
		self.origin = self.config['origin']['type']
		if self.origin == 'gspreadsheet':
			# GSPREAD STUFF
			import gspread
			from oauth2client.service_account import ServiceAccountCredentials
			
			scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
			creds = ServiceAccountCredentials.from_json_keyfile_name('./secrets.json', scope)
			self.client = gspread.authorize(creds)
			self.url = self.config['origin']['url']
			self.number_fields = self.config['origin']['number_fields']
			self.save_locally = self.config['backup']['save_locally']

		# Options
		self.incremental_save = self.config['backup']['incremental_save']
		self.save_to_repo = self.config['backup']['save_to_repo']

		# Database
		self.fields = ['Name', 'ID', 'Original Stock', 'New Stock', 'Change']
		self.database = self.read_database()
		self.datatable = self.make_data_table()
		self.empty_datatable = self.datatable.source.data
		self.items = self.get_items()

		self.busy = False

	def std_out(self, msg):
		if self.verbose:
			print (f'[STD_OUT]: {msg}')
			if not self.logging: return
			with open (join(dirname(__file__), 'logfile.log'), 'a') as logfile:
				logfile.write(f'{strftime("%Y-%m-%d_%H%M%S", localtime())}: {str(msg)}\n')

	def get_items(self):
		items = list()

		for item in self.database.index:
			items.append((item, self.database.loc[item, 'Name']))

		return items

	def get_item_quantity(self, item):
		return self.database.loc[item, 'Current Stock']

	def read_database(self):
		self.std_out ('Reading database')

		if self.origin == 'csv':
			local_file = join(dirname(__file__), 'data/inventory.csv')
			if exists(local_file):
				df = pd.read_csv(local_file)
				self.headers = df.columns
			else:
				self.std_out("Local file doesn't exist")

		elif self.origin == 'gspreadsheet':
			self.std_out (f'Reading from gspreadsheet, using {self.url}')
			
			# Access our google sheets
			sheet = self.client.open_by_url(self.url)
			self.worksheet = sheet.worksheet("STOCK")
			all_values = self.worksheet.get_all_values()

			# Clean up using the length of the first row
			empty_row = list()
			[empty_row.append('') for index in range(len(all_values[0]))]
			if empty_row in all_values: all_values.remove(empty_row)

			# Get headers
			first_line = all_values[0][0:self.number_fields-1]

			self.headers = list(map(lambda s: s.strip(), first_line))
			self.std_out (self.headers)
			del all_values[0]
			parsed_values = list()

			for item in all_values:
				parsed_values.append(item[0:self.number_fields-1])
			df =  pd.DataFrame(parsed_values, columns = self.headers)
			
		df.set_index('ID', inplace=True)
		df = df.astype({"Change": int}, errors='ignore')
		df['Non-submitted changes'] = 0

		df = df[~df.index.duplicated(keep='first')]
		df.drop([i for i in df.columns if 'Unnamed' in i], axis=1, inplace=True)
		return df

	def change_database(self, changed_id, action):

		if changed_id == '': return

		for item in self.items:
			if changed_id == item[0]: changed_item = item[1]

		original_amount = int(self.database.loc[changed_id, 'Original Stock'])
		current_amount = int(self.database.loc[changed_id, 'Current Stock'])
		if action == 'UP': 
			new_amount = current_amount + 1
			self.database.loc[changed_id, 'Non-submitted changes'] += 1
		if action == 'DOWN': 
			new_amount = max(0, current_amount - 1)
			self.database.loc[changed_id, 'Non-submitted changes'] -= 1
		
		self.database.loc[changed_id, 'Current Stock'] = new_amount
		# if new_amount > 0 and changed_id not in self.changed_items: self.changed_items.append(changed_id)

		# Update interface accordingly
		new_data = self.datatable.source.data.copy()
	
		if changed_item in list(new_data['Name']):
			self.std_out ('Item already in list, updating')
			i, = np.where(new_data['Name'] == changed_item)
			new_data['Change'][i] = self.database.loc[changed_id, 'Non-submitted changes']
			new_data['New Stock'][i] = self.database.loc[changed_id, 'Current Stock']
		else:
			new_data['index'] = np.concatenate((new_data['index'], [len(new_data['index'])+1]))
			new_data['Name'] = np.concatenate((new_data['Name'], [changed_item]))
			new_data['ID'] = np.concatenate((new_data['ID'], [changed_id]))
			new_data['Original Stock'] = np.concatenate((new_data['Original Stock'], [self.database.loc[changed_id, 'Original Stock']]))
			new_data['New Stock'] = np.concatenate((new_data['New Stock'], [self.database.loc[changed_id, 'Current Stock']]))
			new_data['Change'] = np.concatenate((new_data['Change'], [self.database.loc[changed_id, 'Non-submitted changes']]))

		self.datatable.source.data = new_data

	def make_data_table(self):
		source_datatable = dict()
		columns_datatable = list()

		for field in self.fields:
			columns_datatable.append(TableColumn(field=field, title=field))
			source_datatable[field] = []

		return DataTable(source=ColumnDataSource(data=pd.DataFrame(source_datatable, columns = self.fields)), columns = columns_datatable, width= 950, height = 320)

	def submit_database(self):
		self.busy = True
		self.std_out ('Submitting')

		if self.origin == 'csv':

			if self.incremental_save: filename = join(dirname(__file__), f'data/inventory_{strftime("%Y-%m-%d_%H%M%S", localtime())}.csv')
			else: filename = join(dirname(__file__), 'data/inventory.csv')

			self.database.to_csv(filename)

		elif self.origin == 'gspreadsheet':
			if self.save_locally:
				self.std_out('Saving locally')
				data_backup = join(dirname(__file__), 'data')

				if not exists(data_backup): os.mkdirs(data_backup)

				if self.incremental_save: filename = join(dirname(__file__), f'data/inventory_{strftime("%Y-%m-%d_%H%M%S", localtime())}.csv')
				else: filename = join(dirname(__file__), 'data/inventory.csv')

				self.database.to_csv(filename)
				self.std_out('Saving locally - Done')
			self.std_out('Saving to gspreadsheet')
			for changed_id in self.database[self.database['Non-submitted changes']!=0].index:

				cell = self.worksheet.find(changed_id)
				columns_to_update = ['Current Stock']
				self.database.loc[changed_id, 'Non-submitted changes'] = 0

				for update in columns_to_update:
					column = self.headers.index(update)
					
				self.worksheet.update_cell(cell.row, self.headers.index('Notes') + 1, 'Last updated by rpi on ' + strftime("%Y-%m-%d %H:%M:%S", localtime()))
			
			self.std_out('gspreadsheet updated')

		if self.save_to_repo: 
			try:
				self.std_out('Save to repository')
				os.chdir(join(dirname(__file__)))
				subprocess.call(['git', 'add', filename])
				subprocess.call(['git', 'commit', '-am', f'"Automated repository changes"'])
				subprocess.call(['git', 'push'])
				self.std_out('Repo backup complete')

			except:
				self.std_out(traceback.print_exc())
				pass

		self.clear_session()
		self.std_out('Success submitting')
	
	def clear_session(self):
		self.busy = True
		self.std_out('Clearing session')
		self.database['Change Temporary'] = 0
		self.datatable.source.data = self.empty_datatable
		self.changed_items = list()
		self.std_out('Cleared')
		self.busy = False

	def request(self, request_input):
		if not self.busy:
			if request_input == 'submit': self.submit_database()
			if request_input == 'clear': self.clear_session()
			if request_input == 'reload': self.read_database()
		else:
			self.std_out('Working on it. Calm down')

def main(doc):
	database = DataBaseHandler(True)

	def callback_up():
		database.change_database(select.value[0], 'UP')
		stock.text = str(database.get_item_quantity(select.value[0]))

	def callback_down():
		database.change_database(select.value[0], 'DOWN')
		stock.text = str(database.get_item_quantity(select.value[0]))

	def callback_select(attr, old, new):
		stock.text = str(database.get_item_quantity(select.value[0]))

	# Select layout
	button_up = Button(label="+", button_type="success", width=480, height=150)
	button_up.on_click(callback_up)
	stock = Div(text='-', css_classes=['output_div'])
	button_down = Button(label="-", button_type="danger", width=480, height=150)
	button_down.on_click(callback_down)
	select = MultiSelect(options=database.items, width=450, height= 400)
	select.on_change('value', callback_select)
	controls = column(button_up, stock, button_down)
	main_layout = layout(row (controls, column(select)), sizing_mode='scale_width')
	main_tab = Panel(child = main_layout, title = 'Select')
	# Main doc
	doc.add_root(Tabs(tabs = [main_tab, submit_tab(database), admin_tab(database)]))
	doc.title = "Fab-Inventory App"

@app.route('/', methods=['GET'])
def bkapp_page():
	script = server_document('http://localhost:5006/main')
	return render_template("index.html", script=script, template="Flask")

def worker():
	# Can't pass num_procs > 1 in this configuration. If you need to run multiple
	# processes, see e.g. flask_gunicorn_embed.py
	server = Server({'/main': main}, io_loop=IOLoop(), allow_websocket_origin=["127.0.0.1:8000"])
	server.start()
	server.io_loop.start()

from threading import Thread
Thread(target=worker).start()

if __name__ == '__main__':
	print('Opening single process Flask app with embedded Bokeh application on http://localhost:8000/')
	print()
	print('Multiple connections may block the Bokeh app in this configuration!')
	app.run(port=8000)